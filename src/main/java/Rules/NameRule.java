package Rules;

import defalut.CheckResult;
import defalut.ICanCheckRule;
import defalut.Person;
import defalut.RuleResult;

public class NameRule implements ICanCheckRule<Person> {

	public CheckResult checkRule(Person entity) {

		boolean hasNumber=false;
		if(entity.getFirstName()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getFirstName().equals(""))
			return new CheckResult("", RuleResult.Error);
		for (int i=0 ;i<entity.getFirstName().length(); i++){
			char c = entity.getFirstName().charAt(i);
			if(Character.isDigit(c))
				hasNumber=true;
		}
		if (hasNumber)
			return new CheckResult("", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}
}
