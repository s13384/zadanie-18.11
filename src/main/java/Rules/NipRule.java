package Rules;

import defalut.CheckResult;
import defalut.ICanCheckRule;
import defalut.Person;
import defalut.RuleResult;

public class NipRule implements ICanCheckRule<Person> {

	@Override
	public CheckResult checkRule(Person entity) {
		
		int suma=10;
		try {
		suma = 6*Character.getNumericValue(entity.getNip().charAt(0)) + 5*Character.getNumericValue(entity.getNip().charAt(1)) + 7*Character.getNumericValue(entity.getNip().charAt(2)) + 
				2*Character.getNumericValue(entity.getNip().charAt(3)) + 3*Character.getNumericValue(entity.getNip().charAt(4)) + 4*Character.getNumericValue(entity.getNip().charAt(5)) +
				5*Character.getNumericValue(entity.getNip().charAt(6)) + 6*Character.getNumericValue(entity.getNip().charAt(7)) + 7*Character.getNumericValue(entity.getNip().charAt(8));
		suma = suma%11;
		if (suma == Character.getNumericValue(entity.getNip().charAt(9)))
			return new CheckResult("",RuleResult.Ok);
		else
			return new CheckResult("",RuleResult.Error);
		} catch (Exception e){
			return new CheckResult("",RuleResult.Error);
		}
	}

}
