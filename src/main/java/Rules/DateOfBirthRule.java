package Rules;

import defalut.CheckResult;
import defalut.ICanCheckRule;
import defalut.Person;
import defalut.RuleResult;

import java.util.Calendar;

import org.joda.time.DateTime;

public class DateOfBirthRule implements ICanCheckRule<Person>{

	@Override
	public CheckResult checkRule(Person entity) {
		try {
			DateTime yob = new DateTime(entity.getDateOfBirth());	
			DateTime peselDate = new DateTime();
			int pesel_year = Integer.parseInt(entity.getPesel().substring(0, 2));
			int pesel_month = Integer.parseInt(entity.getPesel().substring(2, 4));
			int pesel_day =Integer.parseInt( entity.getPesel().substring(4, 6));
			
			peselDate = peselDate.withDayOfMonth(pesel_day);
			
			if (pesel_month >= 81 && pesel_month <= 92){
				peselDate = peselDate.withYear(1800 + pesel_year);
				peselDate = peselDate.withMonthOfYear(pesel_month - 80);
			}
			else if (pesel_month >= 1 && pesel_month <= 12){
				peselDate = peselDate.withYear(1900 + pesel_year);
				peselDate = peselDate.withMonthOfYear(pesel_month);
			}
			else if (pesel_month >= 21 && pesel_month <= 32){
				peselDate = peselDate.withYear(2000 + pesel_year);
				peselDate = peselDate.withMonthOfYear(pesel_month - 20);
			}
			else if (pesel_month >= 41 && pesel_month <= 52){
				peselDate = peselDate.withYear(2100 + pesel_year);
				peselDate = peselDate.withMonthOfYear(pesel_month - 40);
			}
			else if (pesel_month >= 61 && pesel_month <= 72){
				peselDate = peselDate.withYear(2200 + pesel_year);
				peselDate = peselDate.withMonthOfYear(pesel_month - 60);
			}

			DateTime tooday = new DateTime();
			
			CheckResult notBorn = new CheckResult("This person havent born yet", RuleResult.Error);
			CheckResult ok = new CheckResult("", RuleResult.Ok);
			
			if (yob.withTimeAtStartOfDay().isEqual(peselDate.withTimeAtStartOfDay()))
			{
				if (yob.getYear() < tooday.getYear())
				{
					return ok;
				}
				else return notBorn;
				
			}
			else return new CheckResult("", RuleResult.Error);
		}catch(Exception e){
			e.printStackTrace();
			return new CheckResult("", RuleResult.Exception);			
		}
	}

}
