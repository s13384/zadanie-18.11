package tests;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;

import defalut.CheckResult;
import defalut.Person;
import defalut.RuleResult;
import Rules.PasswordRule;

public class PasswordRuleTest {

	PasswordRule rule = new PasswordRule();
	@Test
	public void checker_should_return_error_if_password_is_null() {
		Person p = new Person();
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_password_is_shorter_than_8_characters() {
		Person p = new Person();
		p.setPassword("1234567");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_ok_and_message_password_is_very_weak_if_password_8_characters() {
		Person p = new Person();
		p.setPassword("12345678");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
		Assert.assertEquals("Password is very weak", result.getMessage());
	}
	@Test
	public void checker_should_return_ok_and_message_password_is_very_weak_if_password_11_characters_and_contains_no_letters() {
		Person p = new Person();
		p.setPassword("12345678910");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
		Assert.assertEquals("Password is very weak", result.getMessage());
	}
	@Test
	public void checker_should_return_ok_and_message_password_is_weak_if_password_11_characters_and_contains_letters() {
		Person p = new Person();
		p.setPassword("12345678abc");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
		Assert.assertEquals("Password is weak", result.getMessage());
	}
	@Test
	public void checker_should_return_ok_and_message_password_is_okay_if_password_13_characters_and_contains_letters() {
		Person p = new Person();
		p.setPassword("abcdefghijklm");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
		Assert.assertEquals("Password is okay", result.getMessage());
	}
	@Test
	public void checker_should_return_ok_and_message_password_is_okay_if_password_is_less_than_12_characters_and_contains_letters_and_hashtag() {
		Person p = new Person();
		p.setPassword("abcdefghij#");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
		Assert.assertEquals("Password is okay", result.getMessage());
	}
	@Test
	public void checker_should_return_ok_and_message_password_is_strong_if_password_is_less_than_20_and_more_than_11_characters_and_contains_letters_and_hashtag() {
		Person p = new Person();
		p.setPassword("1234567890#!ab");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
		Assert.assertEquals("Password is strong", result.getMessage());
	}
	@Test
	public void checker_should_return_ok_and_message_password_is_strong_if_password_is_more_than_19_characters_and_contains_letters_and_hashtag() {
		Person p = new Person();
		p.setPassword("1234567890abcdefghi#");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
		Assert.assertEquals("Password is very strong", result.getMessage());
	}

}
