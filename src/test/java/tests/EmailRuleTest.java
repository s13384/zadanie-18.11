package tests;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;

import defalut.CheckResult;
import defalut.Person;
import defalut.RuleResult;
import Rules.EmailRule;

public class EmailRuleTest {

	EmailRule rule = new EmailRule();
	
	@Test
	public void checker_should_return_ok_if_email_is_correct() {
		Person p = new Person();
		p.setEmail("KarolKowalski@jakasdomena.pl");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
	}
	@Test
	public void checker_should_return_ok_if_email_is_correct2() {
		Person p = new Person();
		p.setEmail("julian.iksinski@inna-dom.ena.org");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
	}
	@Test
	public void checker_should_return_ok_if_email_contains_numbers() {
		Person p = new Person();
		p.setEmail("Karol4Kowalski@jakasdomena.pl");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_email_contains_weird_characters() {
		Person p = new Person();
		p.setEmail("Karol4_^&*!#Kowalski@jakasdomena.pl");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_email_contains_enter() {
		Person p = new Person();
		p.setEmail("Karol" + "\n" +"Kowalski@jakasdomena.pl");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_email_contains_tabulator() {
		Person p = new Person();
		p.setEmail("Karol" + "\t" +"Kowalski@jakasdomena.pl");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_domain_is_not_correct() {
		Person p = new Person();
		p.setEmail("Karol_Kowalski@wrongdomain");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_domain_is_not_correct2() {
		Person p = new Person();
		p.setEmail("Karol_Kowalski@.wrongdomain");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_email_contains_two_monkeys() {
		Person p = new Person();
		p.setEmail("Karol@Kowalski@.wrongdomain");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_email_contains_hashtag() {
		Person p = new Person();
		p.setEmail("E#BezdomSKi@.wrongdomain");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}

}
