package tests;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;

import defalut.CheckResult;
import defalut.Person;
import defalut.RuleResult;
import Rules.NipRule;

public class NipRuleTest {

	NipRule rule = new NipRule();
	
	@Test
	public void checker_should_return_ok_if_nip_is_correct() {
		Person p = new Person();
		p.setNip("1234563218");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_nip_is_not_correct() {
		Person p = new Person();
		p.setNip("1234563219");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_nip_contains_character() {
		Person p = new Person();
		p.setNip("123av63219");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_nip_is_string() {
		Person p = new Person();
		p.setNip("lubieplack");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_nip_is_too_long() {
		Person p = new Person();
		p.setNip("12345632190");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_nip_is_too_short() {
		Person p = new Person();
		p.setNip("123456321");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_nip_null() {
		Person p = new Person();
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_nip_is_empty_string() {
		Person p = new Person();
		p.setNip("");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}


}
