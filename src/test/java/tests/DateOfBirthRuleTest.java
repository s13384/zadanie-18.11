package tests;

import static org.junit.Assert.*;

import java.util.Date;

import junit.framework.Assert;

import org.joda.time.DateTime;
import org.junit.Test;

import defalut.CheckResult;
import defalut.Person;
import defalut.RuleResult;
import Rules.DateOfBirthRule;

public class DateOfBirthRuleTest {

	// pesele są podzielone plusami by lepiej odróżnić datę od reszty peselu
	DateOfBirthRule rule = new DateOfBirthRule();
	@Test
	public void checker_should_return_ok_if_date_and_pesels_date_are_the_same() {
		Person p = new Person();
		DateTime yob = new DateTime();
		yob = yob.withYear(1980);
		yob = yob.withMonthOfYear(12);
		yob = yob.withDayOfMonth(3);
		p.setDateOfBirth(yob);
		p.setPesel("801203" + "123" + "1" + "7");
		CheckResult result = rule.checkRule(p);
		System.out.println(result.getMessage());
		Assert.assertEquals(RuleResult.Ok, result.getResult());
	}
	@Test
	public void checker_should_return_ok_if_date_and_pesels_date_are_the_same2() {
		Person p = new Person();
		DateTime yob = new DateTime();
		yob = yob.withYear(1821);
		yob = yob.withMonthOfYear(6);
		yob = yob.withDayOfMonth(6);
		p.setDateOfBirth(yob);
		p.setPesel("218606" + "123" + "1" + "7");
		CheckResult result = rule.checkRule(p);
		System.out.println(result.getMessage());
		Assert.assertEquals(RuleResult.Ok, result.getResult());
	}
	@Test
	public void checker_should_return_ok_if_date_and_pesels_date_are_the_same3() {
		Person p = new Person();
		DateTime yob = new DateTime();
		yob = yob.withYear(2002);
		yob = yob.withMonthOfYear(10);
		yob = yob.withDayOfMonth(20);
		p.setDateOfBirth(yob);
		p.setPesel("023020" + "123" + "1" + "7");
		CheckResult result = rule.checkRule(p);
		System.out.println(result.getMessage());
		Assert.assertEquals(RuleResult.Ok, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_date_and_pesels_date_are_not_the_same() {
		Person p = new Person();
		DateTime yob = new DateTime();
		yob = yob.withYear(1980);
		yob = yob.withMonthOfYear(12);
		yob = yob.withDayOfMonth(3);
		p.setDateOfBirth(yob);
		p.setPesel("800412" + "123" + "1" + "7");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_date_and_pesels_date_are_not_the_same2() {
		Person p = new Person();
		DateTime yob = new DateTime();
		yob = yob.withYear(1980);
		yob = yob.withMonthOfYear(12);
		yob = yob.withDayOfMonth(3);
		p.setDateOfBirth(yob);
		p.setPesel("810312" + "123" + "1" + "7");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_if_date_and_pesels_date_are_not_the_same3() {
		Person p = new Person();
		DateTime yob = new DateTime();
		yob = yob.withYear(1980);
		yob = yob.withMonthOfYear(12);
		yob = yob.withDayOfMonth(3);
		p.setDateOfBirth(yob);
		p.setPesel("800313" + "123" + "1" + "7");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_error_and_message_person_is_not_born_if_date_and_pesels_date_are_the_same_but_year_is_too_high() {
		Person p = new Person();
		DateTime yob = new DateTime();
		yob = yob.withYear(2016);
		yob = yob.withMonthOfYear(1);
		yob = yob.withDayOfMonth(13);
		p.setDateOfBirth(yob);
		p.setPesel("16" + "21" + "13" + "123" + "1" + "7");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
		Assert.assertEquals("This person havent born yet", result.getMessage());
	}

}
