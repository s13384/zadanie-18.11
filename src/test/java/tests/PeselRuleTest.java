package tests;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;

import defalut.CheckResult;
import defalut.Person;
import defalut.RuleResult;
import Rules.PeselRule;

public class PeselRuleTest {

	PeselRule rule = new PeselRule();
	
	@Test
	public void checker_should_retun_error_if_pesel_is_not_correct() {
		Person p = new Person();
		p.setPesel("44051401358");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Error, result.getResult());
	}
	@Test
	public void checker_should_return_ok_if_pesel_is_correct() {
		Person p = new Person();
		p.setPesel("44051401359");
		CheckResult result = rule.checkRule(p);
		Assert.assertEquals(RuleResult.Ok, result.getResult());
	}

}
